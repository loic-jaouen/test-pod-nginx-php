# POD management - has to be started first                                                                       
###############################################################################                
POD=test-php-nginx-pod

.PHONY: pod-create                                                                                                      
pod-create: ## 1. POD: create the pod hosting nginx and php                                                           
	podman pod create --name ${POD} -p 8080:80 -p 8443:443                                                                                                                                                    

.PHONY: pod-kill                                                                                          
pod-kill: ## 1. POD: kill the pod
	podman pod kill ${POD}

.PHONY: pod-rm                                                                                                                                                                                               
pod-rm: ## 1. POD: remove the pod
	podman pod rm ${POD}

.PHONY: pod-ps                                                                                                     
pod-ps: ## 1. POD: process status
	podman pod ps

# PHP
###############################################################################                
CONT_PHP=test-php-container

# start php, mount shared html folder to default /var/www/html
.PHONY: php-run
php-run: ## 2. php: start
	podman run \
		-d --restart=always --pod=${POD} \
		-v ${PWD}/html:/var/www/html:rw,z \
		--name=${CONT_PHP} docker.io/library/php:fpm

.PHONY: php-kill
php-kill: ## 2. php: kill
	podman kill ${CONT_PHP}

.PHONY: php-rm
php-rm: ## 2. php: rm
	podman rm ${CONT_PHP}

# NGINX
###############################################################################                
CONT_NGINX=test-nginx-container

.PHONY: web-run
web-run: ## 3. run nginx
	podman run \
		-d --restart=always --pod=${POD} \
		-v ${PWD}/default.conf:/etc/nginx/conf.d/default.conf:Z \
		-v ${PWD}/html:/var/www/html:rw,z \
		--name=${CONT_NGINX} docker.io/library/nginx

.PHONY: web-kill
web-kill: ## 4. kill the nginx container
	podman kill ${CONT_NGINX}

.PHONY: web-rm
web-rm: ## 4. remove the nginx container
	podman rm ${CONT_NGINX}

# LOGS
###############################################################################                
.PHONY: logs
logs: ## 5. show logs from nginx and php
	podman logs -tf ${CONT_NGINX} ${CONT_PHP}


.PHONY: help
help: ## 0. this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) | sort -h -k 3

.DEFAULT_GOAL := help
