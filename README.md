# test-pod-nginx-php

Test nginx/php in a podman managed pod

- nginx acts as the web server and delegates the php code execution to:
- php-fpm (fast-CGI process manager) 

Those two entities live in seperate processes.

So they can live in seperate containers.

The third entity is the pod hosting those two containers.

# spin

The Makefile tells you what to do:
```bash
make
```

but it goes like this:
- create the pod  
  ```bash
  make pod-create
  ```
- run the php container:
  ```bash
  make php-run
  ```
- run the web container:
  ```bash
  make web-run
  ```

# check

If all went well, you should see an html web page here: http://localhost:8080/ and a php info generated page here: http://localhost:8080/info.php .

Debug log is accessible with:
```bash
make logs
```
Note: tails and follows (`-tf`) logs, so it is best to run this command on a different terminal. You can terminate ([ctrl]+[c]) and re-run it at will.

# stop

The makefile includes other commands for stopping, deleting.

The makefile doesn't do magic, only wraps podman commands.
